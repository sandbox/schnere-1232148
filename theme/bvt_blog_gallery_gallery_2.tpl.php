<?php
if(!$gallery->preview_size) $gallery->preview_size = "1024x768";
if(!$gallery->thumbnail_size) $gallery->thumbnail_size = "150x150";
$preview_size = explode("x",$gallery->preview_size);
$thumbnail_size = explode("x",$gallery->thumbnail_size);
?>

<div class="bvt_blog_gallery_gallery_2 bvt_blog_gallery_gallery_2-<?php print $gallery->id;?>">

  <?php $item_count = 1; ?>
  <?php foreach($gallery_items as $item):?>
    <?php  
      $file = file_load($item->preview_fid);
      $preview_url = file_create_url($file->uri);
      $file = file_load($item->thumbnail_fid);
      $thumbnail_url = file_create_url($file->uri);
      $file = file_load($item->fid);
      $image_url = file_create_url($file->uri);
    ?>
    <?php if($item_count === 1):?>
      <div class="item">
      <img src="<?php print $preview_url;?>" class="item" style="width: <?php print $preview_size[0];?>px;max-height: <?php print $preview_size[1];?>px;" />
      <div class="description" <?php if($item_count !== 1):?>style="display:none;"<?php endif;?>>    
        <div class="text">(<?php print $item_count;?>/<?php print count($gallery_items);?>) Hier sehen Sie ein Bild. Lol. :D.</div>
      </div>
      </div>
    <?php endif; ?>
    
    <?php if($item_count !== 1):?>
      <div class="thumbnail" style="width: <?php print $thumbnail_size[0];?>px;height: <?php print $thumbnail_size[1];?>px;">
        <img src="<?php print $thumbnail_url;?>" class="thumbnail" style="width: <?php print $thumbnail_size[0];?>px;height: <?php print $thumbnail_size[1];?>px;" />
        <div class="description" style="display:none;">    
          <div class="text">(<?php print $item_count;?>/<?php print count($gallery_items);?>) Hier sehen Sie ein Bild. Lol. :D.</div>
        </div>
      </div>
    <?php endif; ?>
    <?php $item_count++; ?>
  <?php endforeach; ?>
</div>
<div style="clear:left;"></div>