<div class="bvt_blog_gallery_gallery_1 bvt_blog_gallery_gallery_1-<?php print $gallery->id;?>">

  <?php $item_count = 1; ?>
  <?php foreach($gallery_items as $item):?>
    <?php  
      $file = file_load($item->preview_fid);
      $preview_url = file_create_url($file->uri);
    ?>
    <img src="<?php print $preview_url;?>" class="item" <?php if($item_count !== 1):?>style="display:none;"<?php endif;?> />
    <div class="description" <?php if($item_count !== 1):?>style="display:none;"<?php endif;?>>
      <div class="prev"><img src="<?php print url(drupal_get_path('module', 'bvt_blog_gallery')) . "/images/arrow_left.png";?>" /></div>
      <div class="text">(<?php print $item_count;?>/<?php print count($gallery_items);?>) Hier sehen Sie ein Bild. Lol. :D.</div>
      <div class="next"><img src="<?php print url(drupal_get_path('module', 'bvt_blog_gallery')) . "/images/arrow_right.png";?>" /></div>
    </div>
    <?php $item_count++; ?>
  <?php endforeach; ?>
</div>
