<?php
/**
 * @file
 * System module integration.
 */

/**
 * Implements hook_form_alter().
 */
function bvt_blog_gallery_form_alter(&$form, &$form_state, $form_id) {

  drupal_alter('bvt_blog_gallery_form_alter', $form, $form_state, $form_id);
}
