<?php
/**
 * @file
 * Node module integration.
 */

/**
 * Implements hook_bvt_blog_gallery_form_alter_alter() on behalf of node.module.
 */
function node_bvt_blog_gallery_form_alter_alter(&$form, &$form_state, $form_id) {
  
  drupal_add_css(drupal_get_path('module', 'bvt_blog_gallery') . '/css/main.css');
  $node_type = null;
  if(isset($form["#node"]))
  {
    $node = $form["#node"];
    $node_type = $node->type;
  }
  if(isset($form["#node_type"]))
  {
    $node_type = $form["#node_type"]->type;
  }

  switch($form_id)
  {
    case 'node_type_form':
      $form['#submit'][] = 'bvt_blog_gallery_node_type_form_submit';
      $bvt_blog_gallery_active_val = variable_get("bvt_blog_gallery_active_{$node_type}",0);
      $bvt_blog_gallery_preview_size = variable_get("bvt_blog_gallery_preview_size_{$node_type}","640x480");
      $bvt_blog_gallery_thumbnail_size = variable_get("bvt_blog_gallery_thumbnail_size_{$node_type}","100x100");
      break;
    case $node_type .'_node_form':
      $enabled = variable_get("bvt_blog_gallery_active_{$node_type}",0);
      $form['#submit'][] = 'bvt_blog_gallery_node_form_submit';
      $bvt_blog_gallery_preview_size = variable_get("bvt_blog_gallery_preview_size_{$node_type}",null);
      $bvt_blog_gallery_preview_size = ($bvt_blog_gallery_preview_size) ? 
                  $bvt_blog_gallery_preview_size : 
                  variable_get("bvt_blog_gallery_preview_size_{$node_type}","640x480");
      $bvt_blog_gallery_thumbnail_size = (isset($bvt_blog_gallery_thumbnail_size)) ? 
                  $bvt_blog_gallery_thumbnail_size :
                  variable_get("bvt_blog_gallery_thumbnail_size_{$node_type}","100x100");
      break;
    
  }

  if ($form_id == 'node_type_form' || ($form_id == $node_type .'_node_form' && $enabled)) 
  {
    $form['bvt_blog_gallery'] = array(
      '#type' => 'fieldset',
      '#title' => 'bvtBlogGallery '.t('settings'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#group' => 'additional_settings',
    );
    
    }

  // enable bvtBlogGallery for this node type
  if($form_id == 'node_type_form')
  {
    $form['bvt_blog_gallery']['bvt_blog_gallery_active'] = array(
        '#type'   => 'checkbox',
        '#title'  => t('Enabled for this node type'),
        '#default_value' => $bvt_blog_gallery_active_val,
    );
  }

  // image preview size
  if ($form_id == 'node_type_form') 
  {
    $form['bvt_blog_gallery']['bvt_blog_gallery_preview_size'] = array(
         '#type'   => 'textfield',
         '#title'  => t('Size of preview image (px)'),
         '#default_value' => $bvt_blog_gallery_preview_size,
    );
    $form['bvt_blog_gallery']['bvt_blog_gallery_thumbnail_size'] = array(
         '#type'   => 'textfield',
         '#title'  => t('Size of thumbnail image (px)'),
         '#default_value' => $bvt_blog_gallery_thumbnail_size,
    );
  }

  // Button to add a new gallery
  if ($form_id == $node_type .'_node_form' && $enabled)
  {
    $form['#attributes'] = array('enctype' => "multipart/form-data");
    $form['bvt_blog_gallery']['bvt_blog_gallery_add_gallery'] = array(
         '#type'   => 'button',
         '#title'  => t('Add Gallery'),
         '#value' => t('Add Gallery'),
         '#ajax' => array(
            'callback'  => 'ajax_bvt_blog_gallery_node_form_add_gallery_submit',
            'wrapper'   => 'bvt_blog_gallery_galleries-wrapper',
            'effect'    => 'fade',
            'method'    => 'append',
          ),
    );

    $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'] = array(
      '#type' => 'fieldset',
      '#title' => t('Galleries'),
      '#prefix' => '<div class="clear-block" id="bvt_blog_gallery_galleries-wrapper">',
      '#suffix' => '</div>',
      '#description' => t('Galleries added to this node'),
      '#collapsible' => TRUE,
      '#collapsed' => false,
    );

    $form['bvt_blog_gallery']['bvt_blog_gallery_galleries_edit'] = array(
      '#type' => 'hidden',
      '#prefix' => '<div id="bvt_blog_gallery_ajax_description_edit" style="display:none;" title="'.t('Description').'"><textarea type="text" name="bvt_blog_gallery_ajax_description_edit_text"></textarea>',
      '#suffix' => '</div>',
    );

    // add new gallery
    if(isset($form_state['triggering_element']) && $form_state['triggering_element']['#id'] == "edit-bvt-blog-gallery-add-gallery")
    {
       $new_gallery_id = bvt_blog_gallery_db_insert($form);
       $form_state['bvt_blog_gallery_added_gallies'] = array();
       $form_state['bvt_blog_gallery_added_gallies'][] = $new_gallery_id;
    }
    // get gallery list for this node
    $galleries = bvt_blog_gallery_db_get_galleries($form['nid']['#value']);

    foreach($galleries as $gallery)
    {
      // upload images
      bvt_blog_gallery_image_upload($form, $form_state,$gallery);

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id] = array(
        '#type' => 'fieldset',
        '#title' => t('Gallery'),
        '#description' => t('Gallery').' ('.$gallery->id.') {bvt_blog_gallery|'.$gallery->id.'}',
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['gallery_id_'.$gallery->id] = array(
        '#type'   => 'hidden',
        '#value'  => $gallery->id,
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['bvt_blog_gallery_preview_size_'.$gallery->id] = array(
        '#type'   => 'textfield',
        '#title'  => t('Size of preview image (px)'),
        '#default_value' => $gallery->preview_size,
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['bvt_blog_gallery_thumbnail_size_'.$gallery->id] = array(
        '#type'   => 'textfield',
        '#title'  => t('Size of thumbnail image (px)'),
        '#default_value' => $gallery->thumbnail_size,
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['bvt_blog_gallery_view_mode_'.$gallery->id] = array(
        '#type'   => 'select',
        '#title'  => t('view'),
        '#default_value' => $gallery->view_mode,
        '#options' => array( 'content' => t('content'), 'block' => t('block')),
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id] = array(
        '#type' => 'fieldset',
        '#title' => t('Items'),
        '#prefix' => '<div class="clear-block bvt_blog_gallery_items-wrapper" id="bvt_blog_gallery_items-wrapper-'.$gallery->id.'">',
        '#suffix' => '</div>',
        '#description' => t('Items added to this gallery'),
        '#tree' => true
      );

      $result = db_query("SELECT * FROM {bvt_blog_gallery_items} WHERE gid=:gid ORDER BY sorting,id ASC",array(':gid' => $gallery->id));
      $records = $result->fetchAll();

      $sorting = 0;
      foreach($records as $item)
      {
        $file = file_load($item->icon_fid);
        $sorting = ($item->sorting) ? $item->sorting : ++$sorting;
        $icon_url = file_create_url($file->uri);
        $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id][$item->fid] = array(
        '#type' => 'fieldset',
        '#title' => t('Items'),
        '#prefix'  => '<div class="bvt_blog_gallery_item_image"><img src="'.$icon_url.'" />',
        '#suffix' => '<a href="#" class="edit">'.t('edit').'</a><br/><a href="#" class="delete">'.t('delete').'</a></div>',
        );
        $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id][$item->fid]["description"] = array(
          '#type'   => 'hidden',
          '#value'  => $item->description,
          
        );
        $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id][$item->fid]["sorting"] = array(
          '#type'   => 'hidden',
          '#value'  => $sorting,
        );
        $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id][$item->fid]["delete"] = array(
          '#type'   => 'hidden',
          '#value'  => 0,
        );
        
      }

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['add_item_item_'.$gallery->id] = array(
        '#type'   => 'file',
        '#title'  => t('Add item'),
        '#prefix' => '<div id="edit-add-item-item-wrapper-'.$gallery->id.'">',
        '#suffix' => '</div>',
        '#description'  => t('Select item to add'),
      );

      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['add_item_'.$gallery->id] = array(
         '#type'   => 'button',
         '#title'  => t('Add Item'),
         '#value' => t('Add Item'),
         '#ajax' => array(
            'callback'  => 'ajax_bvt_blog_gallery_node_form_add_item_submit',
          ),
      );
      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['delete_gallery'] = array(
         '#type'   => 'button',
         '#title'  => t('Delete gallery'),
         '#value' => t('Delete gallery'),
      );
      $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['delete_gallery_'.$gallery->id] = array(
          '#type'   => 'hidden',
          '#value'  => 0,
        );
    }
  }
}

/**
 * Submit callback; save node type settings
 */
function bvt_blog_gallery_node_type_form_submit($form, &$form_state) {
 field_cache_clear();
}

/**
 * Submit callback; save node settings
 */
function bvt_blog_gallery_node_form_submit($form, &$form_state) {
 field_cache_clear();
//   print_r($form['bvt_blog_gallery']);exit;
}

/**
 * Submit callback; save node settings
 */
function ajax_bvt_blog_gallery_node_form_add_gallery_submit($form, &$form_state) {

  $gallery_id = $form_state['bvt_blog_gallery_added_gallies'][0];
  unset($form_state['bvt_blog_gallery_added_gallies']);
  return $form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery_id];
}
/**
 * Submit callback; delete gallery
 */
function ajax_bvt_blog_gallery_node_form_delete_gallery_submit($form, &$form_state) {
  print_r($form_state['triggering_element']);
}
 
function bvt_blog_gallery_db_insert($form)
{
  $node_type = null;
  if(isset($form["#node"]))
  {
    $node = $form["#node"];
    $node_type = $node->type;
  }
  if(isset($form["#node_type"]))
  {
    $node_type = $form["#node_type"]->type;
  }

  $preview_size = variable_get("bvt_blog_gallery_preview_size_{$node_type}","640x480");

  $txn = db_transaction();
  try {
    $id = db_insert('bvt_blog_gallery')
      ->fields(array(
        'nid' => $form['nid']['#value'],
        'uid' => $form['uid']['#value'],
        'preview_size'  => $preview_size
      ))
      ->execute();

    return $id;
  }
  catch (Exception $e) {
    // Something went wrong somewhere, so roll back now.
    $txn->rollback();
    // Log the exception to watchdog.
    watchdog_exception('type', $e);
  }
}

function bvt_blog_gallery_db_get_galleries($nid)
{
  return db_query("SELECT * FROM {bvt_blog_gallery} WHERE nid = :nid",array(':nid' => $nid));
}

function bvt_blog_gallery_db_get_gallery($gid)
{
  $result = db_query("SELECT * FROM {bvt_blog_gallery} WHERE id = :id",array(':id' => $gid));
  return $result->fetchObject();
}

function bvt_blog_gallery_db_get_gallery_items($gallery)
{
  $result = db_query("SELECT * FROM {bvt_blog_gallery_items} WHERE gid=:gid ORDER BY sorting,id ASC",array(':gid' => $gallery->id));
  return $result->fetchAll();
}

/**
 * ajax callback for file upload
 * replaces upload field with empty value version
 * and adds item to view list
*/
function ajax_bvt_blog_gallery_node_form_add_item_submit($form, &$form_state)
{
  global $user;

  $node_type = null;
  if(isset($form["#node"]))
  {
    $node = $form["#node"];
    $node_type = $node->type;
  }
  if(isset($form["#node_type"]))
  {
    $node_type = $form["#node_type"]->type;
  }

  $commands = array();
  
  foreach($form_state['bvt_blog_gallery_added_items'] as $aAddedItem)
  {
    $gallery = $aAddedItem['gallery'];
    $file = $aAddedItem['file'];
    $commands[] = ajax_command_replace('#edit-add-item-item-wrapper-'.$gallery->id, drupal_render($form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['add_item_item_'.$gallery->id]));
    $commands[] = ajax_command_append('#bvt_blog_gallery_items-wrapper-'.$gallery->id . " .fieldset-wrapper:first" , drupal_render($form['bvt_blog_gallery']['bvt_blog_gallery_galleries'][$gallery->id]['items_'.$gallery->id][$file->fid]));
  }

  unset($form_state['bvt_blog_gallery_added_items']);

  return array('#type' => 'ajax','#commands' => $commands);

}

/**
 * upload files and store in database
*/
function bvt_blog_gallery_image_upload(&$form, &$form_state, $gallery) {

    $image_stream = 'public://' . "bvt_blog_gallery/" . $gallery->id ."/";
    $thumbnail_stream = 'public://' . "bvt_blog_gallery/" . $gallery->id ."/thumbnails/";
    $icon_stream = 'public://' . "bvt_blog_gallery/" . $gallery->id ."/icons/";
    $preview_stream = 'public://' . "bvt_blog_gallery/" . $gallery->id ."/preview/";
    if(!is_dir($image_stream))
      drupal_mkdir($image_stream,0755,true);
    if(!is_dir($preview_stream))
      drupal_mkdir($preview_stream,0755,true);
    if(!is_dir($thumbnail_stream))
      drupal_mkdir($thumbnail_stream,0755,true);
    if(!is_dir($icon_stream))
      drupal_mkdir($icon_stream,0755,true);

    $file = file_save_upload('add_item_item_'.$gallery->id, array(), 'public://' . "bvt_blog_gallery/" . $gallery->id ."/" );

    if($file)
    {
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      if(!$gallery->preview_size) $gallery->preview_size = "640x480";
      if(!$gallery->thumbnail_size) $gallery->thumbnail_size = "100x100";
      $preview_size = explode("x",$gallery->preview_size);
      $thumbnail_size = explode("x",$gallery->thumbnail_size);
      $form_state['bvt_blog_gallery_added_items'] = array();
      $form_state['bvt_blog_gallery_added_items'][] = array(
        "gallery" => $gallery,
        "file"    => $file,
      );
      
      $thumbnail = file_copy($file,$thumbnail_stream,FILE_EXISTS_REPLACE);
      $preview = file_copy($file,$preview_stream,FILE_EXISTS_REPLACE);
      $icon = file_copy($file,$icon_stream,FILE_EXISTS_REPLACE);

      $thumbnail_image = image_load(drupal_realpath($thumbnail->uri));
      $preview_image = image_load(drupal_realpath($preview->uri));
      $icon_image = image_load(drupal_realpath($icon->uri));

      image_scale_and_crop($thumbnail_image, $thumbnail_size[0],$thumbnail_size[1]);
      image_scale_and_crop($icon_image, 150,150);
      image_resize($preview_image, $preview_size[0],$preview_size[1]);

      
      image_gd_save($thumbnail_image,drupal_realpath($thumbnail->uri));
      image_gd_save($preview_image,drupal_realpath($preview->uri));
      image_gd_save($icon_image,drupal_realpath($icon->uri));

      $icon_url = file_create_url($icon->uri);

      $fields = array(
        'gid' => $gallery->id,
        'fid' => $file->fid,
        'thumbnail_fid' => $thumbnail->fid,
        'preview_fid' => $preview->fid,
        'icon_fid' => $icon->fid,
      );
      db_insert('bvt_blog_gallery_items')->fields($fields)->execute();
    }
}
