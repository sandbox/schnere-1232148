bvtBlogGallery provides a picture gallery to insert in a blog page (or every
other node type). 

--------------------------------------------------------------------------------

Special thanks to SexyBookmarks [1] and GalleryAssisst [2] Modules. 
Their source code helped me to develop this module.

[1] http://drupal.org/project/sexybookmarks
[2] http://drupal.org/project/gallery_assist