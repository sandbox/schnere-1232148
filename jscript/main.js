function makeSortable(){
 // drag and drop image sorting
  jQuery('.bvt_blog_gallery_items-wrapper .fieldset-wrapper').sortable({
    update: function(event, ui) {
      var iItemCount = 0;
      jQuery(this).closest('.bvt_blog_gallery_items-wrapper').find('.bvt_blog_gallery_item_image').each(function() {
        iItemCount++;
        jQuery(this).find("input[name$='[sorting]']").val(iItemCount);
      });
    },
    items: "div:not(.fieldset-description)",
    placeholder: "bvt_blog_gallery_item_image_drag_placeholder"
  }); 
}

jQuery(document).ready(function(){

  // node edit 
  
  // set gallery for deletion
  jQuery('input[id^="edit-delete-gallery"]').live('click',function() {
    jQuery('input[name^="delete_gallery_"]').val(1);
    return false;
  });
  
  jQuery( ".bvt_blog_gallery_items-wrapper .fieldset-wrapper" ).disableSelection();
  
  jQuery('.bvt_blog_gallery_item_image').live('mouseover',function() {
    jQuery(this).find('a.edit').show();
    jQuery(this).find('a.delete').show();
    makeSortable();
  });
  jQuery('.bvt_blog_gallery_item_image').live('mouseout',function() {
    jQuery(this).find('a.edit').hide();
    jQuery(this).find('a.delete').hide();
  });
  
  jQuery('.bvt_blog_gallery_item_image .delete').live('click',function () {
    jQuery(this).closest('.bvt_blog_gallery_item_image').hide();
    jQuery(this).closest('.bvt_blog_gallery_item_image').find("input[name$='[delete]']").val(1);
    return false;
  });
  jQuery('.bvt_blog_gallery_item_image .edit').live('click',function () {
    var description_field = jQuery(this).closest('.bvt_blog_gallery_item_image').find("input[name$='[description]']");
    jQuery('#bvt_blog_gallery_ajax_description_edit').find('textarea[name=bvt_blog_gallery_ajax_description_edit_text]').html(description_field.val().replace(/\<br\/\>/g,'\n'));
    jQuery('#bvt_blog_gallery_ajax_description_edit').dialog({
      modal: true,
      minWidth: 500,
      minHeight: 200,
      buttons: {
        Ok: function() {
          description_field.val(jQuery('#bvt_blog_gallery_ajax_description_edit').find('textarea[name=bvt_blog_gallery_ajax_description_edit_text]').html().replace(/\r/g, '').replace(/\n/g, '<br/>'));
          jQuery( this ).dialog( "close" );
        }
      }
    });
    return false;
  });
  
  // bvt_blog_gallery_gallery_1
  jQuery(".bvt_blog_gallery_gallery_1 .prev").click(function(){
    var item = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:visible:first");
    var description = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:visible:first").next(".description");
    item.hide();
    description.hide();
    prev_description = item.prev(".description");
    if(prev_description.length == 0)
      prev_description = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:last");
    prev_item = item.prev(".description").prev(".item");
    if(prev_item.length == 0)
      prev_item = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".description:last");
    prev_description.show();
    prev_item.show();
  });
  jQuery(".bvt_blog_gallery_gallery_1 .next").click(function(){
    var item = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:visible:first");
    var description = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:visible:first").next(".description");
    item.hide();
    description.hide();
    prev_description = description.next(".item").next(".description").show();
    if(prev_description.length == 0)
      prev_description = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".item:first");
    prev_item = description.next(".item").show();
    if(prev_item.length == 0)
      prev_item = jQuery(this).closest(".bvt_blog_gallery_gallery_1").find(".description:first");
    prev_description.show();
    prev_item.show();
  });
});